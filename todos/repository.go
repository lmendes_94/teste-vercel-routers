package todos

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

//Repository ...
type Repository struct {
	client *mongo.Client
	ctx    context.Context
}

// Find ...
func (r *Repository) Find() ([]Todo, error) {
	var collection = r.client.Database("todos").Collection("todos")

	cursor, err := collection.Find(r.ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	var result []Todo
	defer cursor.Close(r.ctx)

	if err := cursor.All(r.ctx, &result); err != nil {
		fmt.Errorf("Failed to fetch results: %v", err.Error())
		return nil, err
	}

	return result, nil
}

// Close ...
func (r *Repository) Close() error {
	return r.client.Disconnect(r.ctx)
}

// NewRepository ...
func NewRepository(context context.Context, client *mongo.Client) *Repository {
	return &Repository{
		client: client,
		ctx:    context,
	}
}
