package todos

import "go.mongodb.org/mongo-driver/bson/primitive"

// Todo ...
type Todo struct {
	ID          primitive.ObjectID `bson:"_id" json:"id"`
	Active      bool               `bson:"active" json:"active"`
	Description string             `bson:"description" json:"description"`
}
